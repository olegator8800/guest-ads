export default {
  path: {
    downloadAdsAsCsv: '/api/ads/report/download',
    getAdsList: '/api/ads/',
    createAd: '/api/ads/'
  }
}
