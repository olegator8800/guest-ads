guest-ads
======

#### Как развернуть
* Запуск docker окружения:

```bash
./build/run.sh
```
* Создать `.env` по аналогии с `.env.dist`

#### Внимание
* Adblock:
Отключить Adblock т.к. он блокирует все запросы на ^/ads/

#### Тесты
* Запуск тестов:
```bash
docker exec -ti guest-ads-php-fpm php vendor/bin/codecept run
```
