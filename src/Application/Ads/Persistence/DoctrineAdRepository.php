<?php
declare(strict_types=1);

namespace App\Application\Ads\Persistence;

use Doctrine\ORM\EntityManagerInterface;
use App\Domain\Ads\AdRepositoryInterface;
use App\Domain\Ads\Ad;

class DoctrineAdRepository implements AdRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function findAdsOrderedByCreatedAt(int $offset = 0, int $limit = 30): array
    {
        $entity = Ad::class;

        $query = "SELECT a FROM {$entity} a
                  ORDER BY a.createdAt DESC";

        return $this->entityManager
            ->createQuery($query)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getResult()
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Ad $ad): void
    {
        $em = $this->entityManager;

        $em->persist($ad);
        $em->flush();
    }
}
