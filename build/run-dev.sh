#!/usr/bin/env bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if ! docker ps -q &> /dev/null
then
    echo "You must be in docker group or root"
    exit 1
fi

docker network create guest-ads

cd ${DIR}/

docker-compose --project-name guest-ads up --build -d guest-ads-nginx
docker-compose --project-name guest-ads run --rm guest-ads-php-fpm /usr/bin/composer install --no-plugins --no-scripts
docker-compose --project-name guest-ads run --rm guest-ads-php-fpm chown -R `id -u $USER`:`id -u $GROUP` .
