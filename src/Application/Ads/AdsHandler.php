<?php
declare(strict_types=1);

namespace App\Application\Ads;

use Ramsey\Uuid\Uuid;
use App\Domain\Ads\AdRepositoryInterface;
use App\Domain\Ads\Ad;
use App\Domain\Ads\Exception\AdsValidationExceptionInterface;
use App\Application\Ads\CreateAdCommand;
use App\Application\Ads\AdDTO;
use App\Application\Ads\AdResultDTO;
use App\Application\Ads\Exception\AdsValidationException;

class AdsHandler
{
    /**
     * @var AdRepositoryInterface
     */
    private $adRepository;

    /**
     * @param AdRepositoryInterface $adRepository
     */
    public function __construct(AdRepositoryInterface $adRepository)
    {
        $this->adRepository = $adRepository;
    }

    /**
     * @param  CreateAdCommand $command
     *
     * @return string
     */
    public function createAd(CreateAdCommand $command): string
    {
        $id = Uuid::uuid4();
        $title = $command->title;
        $text = $command->text;

        try {
            $ad = new Ad($id, $title, $text);

            $this->adRepository->save($ad);
        } catch (AdsValidationExceptionInterface $e) {
            throw new AdsValidationException($e->getMessage(), $e->getTextCode(), $e);
        }

        return $id->toString();
    }

    /**
     * @param  int $page
     *
     * @return AdResultDTO
     */
    public function getAdsByPage(int $page): AdResultDTO
    {
        $limit = 5;
        $offset = $this->calculateOffset($page, $limit);

        $list = $this->adRepository->findAdsOrderedByCreatedAt($offset, $limit);

        $adDTOList = array_map(
            function($ad) {
                return AdDTO::createByAd($ad);
            },
            $list
        );

        $adResultDTO = new AdResultDTO();

        $adResultDTO->page = $page;
        $adResultDTO->limit = $limit;
        $adResultDTO->resultCount = count($adDTOList);
        $adResultDTO->adList = $adDTOList;

        return $adResultDTO;
    }

    /**
     * @return Generator
     */
    public function getAdsAsCSV(): \Generator
    {
        $limit = 5; //only for example
        $page = 1;
        $offset = 0;

        $headers = [
            'title',
            'text',
            'created at'
        ];

        while ($list = $this->adRepository->findAdsOrderedByCreatedAt($offset, $limit)) {
            $fp = fopen('php://temp','r+');

            if ($page === 1) {
                fputcsv($fp, $headers);
            }
            $page++;
            $offset = $this->calculateOffset($page, $limit);

            foreach ($list as $ad) {
                $row = [];

                $row[] = $ad->getTitle();
                $row[] = $ad->getText();
                $row[] = ($ad->getCreatedAt())->format('Y-m-d H:i:s');

                fputcsv($fp, $row);
            }

            rewind($fp);
            $csv = stream_get_contents($fp);
            fclose($fp);

            yield $csv;
        }
    }

    private function calculateOffset(int $page, int $limit): int
    {
        return ($page - 1) * $limit;
    }
}
