<?php
declare(strict_types=1);

use Codeception\Util\Stub;
use Helper\Unit;
use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Uuid;

use App\Domain\Ads\Ad;
use App\Domain\Ads\Exception\EmptyTitleException;
use App\Domain\Ads\Exception\EmptyTextException;

class AdTest extends \Codeception\Test\Unit
{
    /**
     * @test
     */
    public function createAdByEmptyTitleException()
    {
        $this->expectException(EmptyTitleException::class);
        $this->expectExceptionMessage(EmptyTitleException::MESSAGE);

        new Ad($this->generateUuid(), $emptyTitle = '', 'text');
    }

    /**
     * @test
     */
    public function createAdByEmptyTextException()
    {
        $this->expectException(EmptyTextException::class);
        $this->expectExceptionMessage(EmptyTextException::MESSAGE);

        new Ad($this->generateUuid(), 'title', $emptyText = '');
    }

    /**
     * @test
     */
    public function adGetTitle()
    {
        $title = 'text text';

        $ad = new Ad($this->generateUuid(), $title, 'text');

        $this->assertEquals($ad->getTitle(), $title);
    }

    /**
     * @test
     */
    public function adGetText()
    {
        $text = 'text text';

        $ad = new Ad($this->generateUuid(), 'title', $text);

        $this->assertEquals($ad->getText(), $text);
    }

    private function generateUuid(): UuidInterface
    {
        return Uuid::uuid4();
    }
}
