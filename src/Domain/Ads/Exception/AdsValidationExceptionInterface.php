<?php
declare(strict_types=1);

namespace App\Domain\Ads\Exception;

interface AdsValidationExceptionInterface extends AdsExceptionInterface
{
    public function getTextCode(): string;
}
