<?php
declare(strict_types=1);

namespace App\Application\Ads;

use App\Domain\Ads\Ad;

class AdDTO
{
    public static function createByAd(Ad $ad)
    {
        $dto = new static();

        $dto->title = $ad->getTitle();
        $dto->text = $ad->getText();
        $dto->createdAt = $ad->getCreatedAt();

        return $dto;
    }

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $text;

    /**
     * @var \DateTimeImmutable
     */
    public $createdAt;
}
