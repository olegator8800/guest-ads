<?php
declare(strict_types=1);

namespace App\Domain\Ads;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use App\Domain\Ads\Exception\EmptyTitleException;
use App\Domain\Ads\Exception\EmptyTextException;

/**
 * @ORM\Entity
 */
class Ad
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     */
    private $text;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @param UuidInterface $id
     * @param string $title
     * @param string $text
     *
     * @throws EmptyTitleException If $title param is empty
     * @throws EmptyTextException If $text param is empty
     */
    public function __construct(UuidInterface $id, string $title, string $text)
    {
        if (!$title = trim($title)) {
            throw new EmptyTitleException();
        }

        if (!$text = trim($text)) {
            throw new EmptyTextException();
        }

        $this->id = $id;

        $this->title = $title;
        $this->text = $text;

        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return \DateTimeImmutable::createFromMutable($this->createdAt);
    }
}
