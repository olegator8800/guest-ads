import React, { Component } from 'react'
import './App.css'
import { 
  Container,
  Row,
  Col,
  Button,
  Jumbotron
} from 'reactstrap'
import AdsList from './AdsList'
import CreateAdModal from './CreateAdModal'
import config from './config'

class App extends Component {
  render () {
    return (
      <Container>
        <Row>
          <Col sm=""></Col>
          <Col sm="8">
            <Jumbotron>
              <h1>Guest Ads</h1>
              <hr />
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              <p>
                <CreateAdModal />
                {" "}
                <Button color="success" href={config.path.downloadAdsAsCsv}>Download as CSV</Button>
              </p>
            </Jumbotron>
          </Col>
          <Col sm=""></Col>
        </Row>
        <Row>
          <Col sm=""></Col>
          <Col sm="8">
            <AdsList />
          </Col>
          <Col sm=""></Col>
        </Row>
        <Row>
          <Col sm=""><br /><br /><br /><br /></Col>
        </Row>
      </Container>
    )
  }
}

export default App
