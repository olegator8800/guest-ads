<?php
declare(strict_types=1);

namespace App\Domain\Ads\Exception;

class EmptyTextException extends \DomainException implements AdsValidationExceptionInterface
{
    const MESSAGE = 'Text cannot be empty';

    /**
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = self::MESSAGE, int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getTextCode(): string
    {
        return 'empty text';
    }
}
