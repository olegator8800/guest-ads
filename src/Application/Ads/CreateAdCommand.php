<?php
declare(strict_types=1);

namespace App\Application\Ads;

class CreateAdCommand
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $text;
}
