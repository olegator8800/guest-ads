import React, { Component } from 'react'
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Form,
  Alert,
  Progress
} from 'reactstrap'


import config from './config'

class CreateAdModal extends Component {
  constructor(props) {
      super(props)

      this.state = {
          modal: false,
          isLoading: false,
          error: '',
          title: '',
          text: ''
      }

      this.toggle = this.toggle.bind(this)
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  toggle() {
      this.setState(prevState => ({
          modal: !prevState.modal
      }))
  }

  isHasError = () => {
    return !!this.state.error
  }

  loadingStart = () => {
    this.setState({
      isLoading: true,
      error: ''
    })
  }

  loadingEnd = (error) => {
    this.setState({
      isLoading: false,
      error: error
    })
  }

  handleSubmit = async (event) => {
    event.preventDefault()

    const { title, text } = this.state

    this.loadingStart()

    const url = config.path.createAd
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        title: title,
        text: text
      })
    })

    if (response.ok) {
      document.location.reload(true)

      this.loadingEnd()

      return
    }

    const data = await response.json()

    this.loadingEnd(data.error)
  }

  render () {
    const { title, text, error, isLoading } = this.state

    let submitBtn = <Button color="primary">Create</Button>

    if (isLoading) {
      submitBtn = <Button color="primary" disabled>Create</Button>
    }

    return (
      <span>
        <Button color="primary" onClick={this.toggle}>Add Ads</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} unmountOnClose={true}>
          <Form onSubmit={this.handleSubmit}>
            <ModalHeader toggle={this.toggle}>Create Ad</ModalHeader>
            <ModalBody>
                <Input
                  name="title"
                  type="input"
                  placeholder="Title"
                  value={title}
                  onChange={this.handleChange}
                />
                <br />
                <Input
                  name="text"
                  type="textarea"
                  placeholder="Text"
                  value={text}
                  onChange={this.handleChange}
                  rows={3}
                />
              
              <br />
              <Alert color="danger" isOpen={this.isHasError()}>
                {error}
              </Alert>
              {isLoading &&
                <Progress animated color="info" value="100" />
              }
            </ModalBody>
            <ModalFooter>
              {submitBtn}
              {' '}
              <Button color="secondary" onClick={this.toggle}>Cancel</Button>
            </ModalFooter>
          </Form>
        </Modal>
      </span>
    )
  }
}

export default CreateAdModal
