<?php
declare(strict_types=1);

namespace App\Domain\Ads;

use App\Domain\Ads\Ad;

interface AdRepositoryInterface
{
    /**
     * @param  int $offset
     * @param  int $limit
     *
     * @return array
     */
    public function findAdsOrderedByCreatedAt(int $offset = 0, int $limit = 30): array;

    /**
     * @param Ad $ad
     */
    public function save(Ad $ad): void;
}
