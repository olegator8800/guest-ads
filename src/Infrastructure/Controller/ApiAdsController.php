<?php
declare(strict_types=1);

namespace App\Infrastructure\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Application\Ads\AdsHandler;
use App\Application\Ads\CreateAdCommand;
use App\Application\Ads\Exception\AdsValidationException;

/**
 * @Route("/api/ads", name="api_ads_")
 */
class ApiAdsController
{
    /**
     * @var AdsHandler
     */
    private $adsHandler;

    /**
     * @param AdsHandler $adsHandler
     */
    public function __construct(AdsHandler $adsHandler)
    {
        $this->adsHandler = $adsHandler;
    }

    /**
     * @Route("/", methods={"GET"}, name="get")
     */
    public function getAds(Request $request)
    {
        $page = (int) $request->query->get('page', 1);

        $result = $this->adsHandler->getAdsByPage($page);

        return $this->createResponse($result);
    }

    /**
     * @Route("/", methods={"POST"}, name="create")
     */
    public function createAds(Request $request)
    {
        try {
            $data = json_decode($request->getContent(), true, $depth = 512, \JSON_THROW_ON_ERROR);
            $request->request->replace(is_array($data) ? $data : array());

            $createAdCommand = new CreateAdCommand();

            $createAdCommand->title = strip_tags($request->request->get('title', ''));
            $createAdCommand->text = strip_tags($request->request->get('text', ''));

            $uuid = $this->adsHandler->createAd($createAdCommand);

            return $this->createResponse(
                [
                    'created' => true,
                    'uuid' => $uuid,
                ],
                Response::HTTP_CREATED
            );
        } catch (\Throwable $e) {
            return $this->createErrorResponse($e);
        }
    }

    /**
     * @Route("/report/download", methods={"GET"}, name="report_download")
     */
    public function downloadReport()
    {
        $fileName = 'ads_report.csv';
        $adsHandler = $this->adsHandler;

        return new StreamedResponse(
            function () use ($adsHandler) {
                $tempFile = fopen('php://output', 'r+b');

                foreach ($adsHandler->getAdsAsCSV() as $csvChunk) {
                    fwrite($tempFile, $csvChunk . "\n");
                    flush();
                }

                fclose($tempFile);
            },
            Response::HTTP_OK,
            [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
            ]
        );
    }

    /**
     * @param mixed $data
     * @param int $statusCode
     *
     * @return Response
     */
    private function createResponse($data, int $statusCode = Response::HTTP_OK): Response
    {
        return new Response(
            json_encode($data),
            $statusCode,
            [
                'Content-Type' => 'application/json',
            ]
        );
    }

    /**
     * @param \Throwable $e
     * @param int $statusCode
     *
     * @return Response
     */
    private function createErrorResponse(\Throwable $e, int $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR): Response
    {
        if ($e instanceof AdsValidationException) {
            return $this->createResponse(
                [
                    'error' => $e->getMessage(),
                    'code' => $e->getTextCode(),
                ],
                $statusCode
            );
        }

        if ($e instanceof \JsonException) {
            return $this->createResponse(
                [
                    'error' => $e->getMessage(),
                    'code' => 'json invalid',
                ],
                $statusCode
            );
        }

        return $this->createResponse(
            [
                'error' => 'Internal error',
                'code' => 'internal error',
            ],
            $statusCode
        );
    }
}
