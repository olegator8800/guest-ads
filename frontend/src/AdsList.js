import React, { Component } from 'react'
import { 
  Button,
  Card,
  CardTitle,
  CardText,
  Progress
} from 'reactstrap'
import config from './config'

class AdsList extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isLoading: true,
      isEmptyList: false,
      moreAdsAvailable: true,
      nextPage: 1,
      ads: []
    }
  }

  loadAds = async () => {
    const page = this.state.nextPage

    this.setState({
      isLoading: true
    })

    const url = `${config.path.getAdsList}?page=${page}`
    const response = await fetch(url)

    const data = await response.json()

    let moreAdsAvailable = true

    if (data.resultCount !== data.limit || data.resultCount === 0) {
      moreAdsAvailable = false
    }

    let isEmptyList = false

    if (page === 1 && data.resultCount === 0) {
      isEmptyList = true
    }

    this.setState({
      moreAdsAvailable: moreAdsAvailable,
      isEmptyList: isEmptyList,
      isLoading: false,
      nextPage: page + 1,
      ads: [...this.state.ads, ...data.adList]
    })
  }

  componentDidMount = () => {
    this.loadAds()
  }

  render () {
    const { moreAdsAvailable, isLoading, ads, isEmptyList } = this.state

    let moreAdsButton = <Button color="secondary" block disabled>More ads</Button>

    if (moreAdsAvailable) {
      moreAdsButton = <Button color="secondary" block onClick={this.loadAds}>More ads</Button>
    }

    const loadBar = <Progress animated color="info" value="100" />

    if (isLoading) {
      return (
        <span>
            {loadBar}
        </span>
      )
    }

    let items = ads.map((item, key) => {
        const createAt = new Date(item.createdAt.date)

        return (
        <span key={key}>
          <Card body>
            <CardTitle>{item.title}</CardTitle>
            <CardText>{item.text}</CardText>
            <CardText>
              <small className="text-muted">
                {createAt.toLocaleDateString("en-US")}
                {" "}
                {createAt.toLocaleTimeString("en-US")}
              </small>
            </CardText>
          </Card>
          <hr />
        </span>
      )
    });

    if (isEmptyList) {
      items = <span>Empty list</span>
      moreAdsButton = ''
    }

    return (
      <span>
        {items}
        {moreAdsButton}
      </span>
    )
  }
}

export default AdsList
