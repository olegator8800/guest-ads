<?php
declare(strict_types=1);

namespace App\Application\Ads;

class AdResultDTO
{
    /**
     * @var int
     */
    public $page;

    /**
     * @var int
     */
    public $limit;

    /**
     * @var int
     */
    public $resultCount;

    /**
     * @var array
     */
    public $adList;
}
