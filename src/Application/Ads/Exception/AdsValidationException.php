<?php
declare(strict_types=1);

namespace App\Application\Ads\Exception;

class AdsValidationException extends \Exception
{
    /**
     * @var string
     */
    private $textCode;

    /**
     * @param string          $message
     * @param string          $textCode
     * @param \Throwable|null $previous
     */
    public function __construct(string $message, string $textCode, \Throwable $previous = null)
    {
        $this->textCode = $textCode;

        parent::__construct($message, 0, $previous);
    }

    /**
     * @return string
     */
    public function getTextCode(): string
    {
        return $this->textCode;
    }
}
